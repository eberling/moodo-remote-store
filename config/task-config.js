module.exports = {
  html        : true,
  images      : true,
  fonts       : true,
  static      : true,
  svgSprite   : true,
  ghPages     : true,
  stylesheets : true,

  javascripts: {
    entry: {
      // files paths are relative to
      // javascripts.dest in path-config.json
      app: ['babel-polyfill', './app.js']
    }
  },

  browserSync: {
    server: {
      open: false,
      // should match `dest` in
      // path-config.json
      baseDir: 'public'
      // middleware: function (req, res, next) {
      //   res.setHeader('cache-control', 'private, max-age=30');
      //   next();
      // }
    }
  },

  production: {
    rev: true
  }
}
