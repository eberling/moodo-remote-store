
// // Data formats
// Item {
//     id: string,
//     text: string,
//     note: string,
//     dateCreated: date,
//     dateModified: date,
//     flags: number
// }

// ItemMap: {
//     "id1": Item,
//     "id2": Item,
//     ...
// }

// API

// // Interface switches
// DataLocal = mobile ? DataLocalReactNative : DataLocalIDB
// DataRemote = legacy ? DataRemoteGAPI : DataRemoteFire

// // Jay: Interface with UI code
// Data {
//     load(onLoadedLocal: function, onLoadedRemote: function) {
//         DataLocal.load((localData: object) => {
//             onLoadedLocal(localData)
//             DataRemote.load(localData, this._dataRemoteLoaded, this._onItemChanged)
//         });
//     }
//     _dataRemoteLoaded(remoteData: object) {
//         foreach(itemData in remoteData)
//             this._applyChangesToItem(itemData)
//     }
//     _onItemChanged(itemData: object) {
//         this._applyChangesToItem(itemData)
//     }
//     _applyChangesToItem(itemData: object) {
//         item = this.getItem(itemData.id);
//         resolve conflicts
//         modify item
//     }
//     save(item: VMLI, changes: object) {
//         itemData:Item = transform changes to db format
//         DataLocal.save(itemData);
//         DataRemote.save(itemData);
//     }
//     beginTransaction() {
//         DataRemote.beginTransaction()
//     }
//     endTransaction() {
//         DataRemote.endTransaction()
//     }
//     moveItem(item: VMLI, parent: VMLI) {...}
//     deleteItem(item: VMLI) {...}
//     getItem(id: string) { return VMLI }
//     getData(id: string) { return itemData }
// }

// // Jay: Local adapters
// DataLocal {
//     load(onLoaded: function) {... }
//     save(itemData: object) {...}
// }

// Remote adapters:
// Steven: the new version of this
// Jay: refactor existing code to be the GAPI version of this

// Responsible for connecting to store and realtime servers and translating the data into ItemMap

class DataRemote {
  constructor() {
    this.cb = { change: [] };
  }

  onChange(fn) { return hook(this.cb.change, fn); }
  offChange(fn) { return unhook(this.cb.change, fn); }

  load (localData/*: object*/) {
    this._localData = localData;

    const storeLoaded = (new StoreAdapter()).load(localData);

    const realtime = new RealtimeAdapter();
    const realtimeLoaded = realtime.load();
    realtime.onChange(this._onRealtimeChanged);

    const loaded = Promise.all([storeLoaded, realtimeLoaded]);

    loaded.then(this._onDoneLoading);

    return loaded;
  }
  _onDoneLoading() {
    // TODO this._remoteData: ItemMap = start with the local data, add the diffs from the store, add the revisions from realtime
    this.cb.load.forEach((cb) => cb(this._remoteData));
  }
  _onRealtimeChanged (itemData/*: Item*/) {
    // itemData contains only the fields that changed
    this.cb.change.forEach((cb) => cb(itemData));
  }
  save (item) {
    RealtimeAdapter.save(item);
  }
  beginTransaction() {
    RealtimeAdapter.beginTransaction();
  }
  endTransaction() {
    RealtimeAdapter.endTransaction();
  }
};

// Responsible for connecting to store and returning data when loaded
class StoreAdapter {
  load (localData/*: object*/, onLoaded/*: function*/) {
    // connect to static DB
    // dataDiff = get diff since localData
    // onLoaded(dataDiff)
  }
};

// Responsible for connecting to realtime database, saving changes to it, and firing events on change
class RealtimeAdapter {
  constructor() {
    this.cb = { change: [] };
  }

  onChange(fn) { return hook(this.cb.change, fn); }
  offChange(fn) { return unhook(this.cb.change, fn); }

  load () {
    return new Promise((resolve, reject) => {
      connectToRealtime((realtimeRevisions/*: object*/) => {
        subscribeToChanges(this._onItemChanged);
        resolve(realtimeRevisions);
      });
    });
  }
  _onItemChanged (itemData/*: Item*/) {
    this.cb.change.forEach((cb) => cb(itemData));
  }
  save (itemData/*: Item*/) {
    // if batching then queue change
    // else save change to realtime database
  }
  beginTransaction () {
    // begin batch
  }
  endTransaction () {
    // save any queued changes as atomic transaction
  }
};
