// TODO pulled from Steven's functional library
// cryptic out of context
// use lodash?
export const assocMutating = (key, val, obj) => Object.assign(obj, { [key]: val });

export const constants = {};

constants.identity = (x) => x;

constants.true = (x) => true;

constants.index = (x, idx) => idx;

export const pluck = (prop, obj) => obj ? obj[prop] : undefined;

export const pluckFn = (prop) => (obj) => pluck(prop, obj);

export const collect = (fnA = constants.identity, fnB = constants.true, memo, x, idx) =>
  assocMutating(fnA(x, idx), fnB(x, idx), memo);

export const mark = (memo, x, idx) =>
  collect(undefined /* default: key */, undefined /* default: true */, memo, x, idx);

export const indexBy = (prop) => (memo, x, idx) =>
  collect(pluckFn(prop), constants.identity, memo, x, idx);

export const positions = (memo, x, idx) =>
  collect(undefined /* default: key */, constants.index, memo, x, idx);

export const doCompose = (fns, a) => {
  const fnsFromEnd = fns.reverse();
  for (let fn of fnsFromEnd) a = fn(a);
  return a;
};

export const compose = (...fns) => (a) => doCompose(fns, a);

export const blacklistProps = (item, ignored) => {
  if (!item) return item;

  const copy = {};
  for (var key in item) {
    if (!(key in ignored)) copy[key] = item[key];
  }
  return copy;
};

export const blacklistPropsFn = (ignored) => (item) => blacklistProps(item, ignored);

export const whitelistProps = (item, kept) => {
  if (!item) return item;

  const copy = {};
  for (var key in item) {
    if (key in kept) copy[key] = item[key];
  }
  return copy;
};

export const whitelistPropsFn = (kept) => (item) => whitelistProps(item, kept);

export const clone = (item) => Object.assign({}, item);

// https://stackoverflow.com/a/8432188export const jsonDiff = (obj1, obj2) => {
export const jsonDiff = (obj1, obj2) => {
  const result = {
    id: obj1.id
  };
  const fn = (key) => {
    if (obj2[key] === undefined) result[key] = null;
    else if (obj1[key] !== obj2[key]) result[key] = obj2[key];
  };

  Object.keys(obj1).forEach(fn);
  Object.keys(obj2).forEach(fn);

  return result;
};

export const assertIn = (map, config = {}) => (id) => {
  if (config.forStringsOnly && typeof id !== 'string') return id;
  // we have an actual key for the map
  if (!(id in map)) throw new Error('Missing value in map: ' + id);
  return id;
};

export const keyToValueAssertFound = (map) =>
  compose(toMap(map), assertIn(map, { forStringsOnly: true }));

export const toMap = (map) => (id) => map[id];
