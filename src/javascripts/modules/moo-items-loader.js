import * as childrenById from './mixins/children-by-id';
import * as localIndex from './mixins/local-index';
import * as remoteIndex from './mixins/remote-index';
import {
  indexBy,
  jsonDiff,
  clone
} from './functional';

const toDiff = (type, prev, curr) => {
  if (type === 'removed' || type === 'added') return { type, value: curr };
  return { type, value: jsonDiff(prev, curr) };
};

const toDiffFn = (store) => (change) =>
  toDiff(change.type, store[change.value.id], change.value);

const toChange = (item, type) => ({ type, value: item });

export const toChangeFn = (type) => (item) => toChange(item, type);

const setOnValue = (change, set) => {
  // could be done with lenses, but this seems less confusing
  change.value = [change.value]
    .map(clone)
    .map(set)[0];
  return change;
};

export const setOnValueFn = (set) => (change) => setOnValue(change, set);

const setDiffOnValueFn = (prevStore, api) => (change) => {
  return [change]
    .map(toDiffFn(prevStore))
    .map(setOnValueFn((item) => {
      return api._encrypter.decrypt(item.text, api._config.crypto.key, api._config.crypto.salt, api._config.crypto.salt)
        .then(text => {
          item.text = text;
          return [item]
            .map(setOnValueFn(localIndex.setFn(api._localIdxs)))[0];
        });
    }));
};

const loadStore = (store, change, localIdxs) => {
  const item = [change.value]
    .map(item => Object.assign(item, { index: localIdxs[item.id] }))[0];
  store = indexBy('id')(store, item);
  if (change.type === 'removed') delete store[item.id];
  return store;
};

const loadStoreFn = (localIdxs) => (store, change) => loadStore(store, change, localIdxs);

const normalizeLoadChanges = ({ api, changes }) => {
  return changes
    .map((c) => c.value)
    .map((item) => {
      return api._encrypter.decrypt(item.text, api._config.crypto.key, api._config.crypto.salt, api._config.crypto.salt)
        .then(text => {
          item.text = text;
          return [item]
            .map(localIndex.setFn(api._localIdxs))[0];
        });
    });
};

const normalizeUpdateChanges = ({ api, changes, prevStore }) => {
  // for changes with parentIdx diff present (or new or deleted), attach local property
  return changes
    .map(setDiffOnValueFn(prevStore, api));
};

const reset = (api) => {
  api._remoteIdxs = {};
  api._children = {};
  api._store = {};
  api._localIdxs = {};
  api._remoteIdxs = {};
  return api;
};

const load = ({ changes, type = 'load', prevStore, api }) => {
  api._remoteIdxs = changes.reduce(remoteIndex.load, api._remoteIdxs);
  api._children = changes.reduce(childrenById[type], api._children);
  const localIdxsLoad = localIndex[`${type}Fn`]({
    store: api._store,
    prevStore,
    remoteIdxs: api._remoteIdxs,
    children: api._children
  });
  api._localIdxs = changes.reduce(localIdxsLoad, api._localIdxs);
  api._store = changes.reduce(loadStoreFn(api._localIdxs), api._store);
  return api;
};

const localUpdate = ({ item, api, changeType }) => {
  const prevStore = Object.assign({}, api._store);
  const change = toChange(item, changeType);
  load({ changes: [change], prevStore, api });
};

const localUpdateReducer = (changeType) => (api, item) =>
  localUpdate({ item, api, changeType });

// minimalist interface to do keyword-based polymorphism
export const updater = {
  load: ({ changes, api, prevStore }) =>
    load({ api, type: 'update', changes, prevStore }),
  normalizeChanges: normalizeUpdateChanges,
  addItem: localUpdateReducer('added'),
  removeItem: localUpdateReducer('removed'),
  modifyItem: localUpdateReducer('modified')
};

export const loader = {
  load: ({ changes, api, prevStore }) => load({ changes, api: reset(api), prevStore }),
  normalizeChanges: normalizeLoadChanges
};

export const _ = {
  toDiff,
  toChange,
  setDiffOnValueFn,
  loadStore,
  normalizeLoadChanges,
  normalizeUpdateChanges,
  load,
  localUpdate
};
