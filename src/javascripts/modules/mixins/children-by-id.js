const collect = (index, item) => {
  if (!index[item.parent]) index[item.parent] = [];
  index[item.parent].push(item.id);
  return index;
};

const collectUniquely = (index, item) => {
  if (index[item.parent] && index[item.parent].includes(item.id)) {
    return index;
  }
  return collect(index, item);
};

const loadInternal = (index, change, collect) => {
  if (change.type !== 'removed') {
    index = collect(index, change.value);
  } else {
    index[change.value.parent] = (index[change.value.parent] || []).filter(e => e !== change.value.id);
  }
  return index;
};

export const load = (index, change) => loadInternal(index, change, collect);

export const update = (index, change) => loadInternal(index, change, collectUniquely);

export const _ = {
  loadInternal
};
