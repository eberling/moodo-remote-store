import * as remoteIndex from './remote-index';
import {
  positions,
  assocMutating,
  clone
} from '../functional';

const collect = (localIdxs, item, remoteIdxs, children, { rebuild } = {}) => {
  // build localIdxs for the parent, if needed
  if (!(item.id in localIdxs) || rebuild) {
    const childIds = (children || []).sort(remoteIndex.byFn(remoteIdxs));
    localIdxs = childIds.reduce(positions, localIdxs);
  }
  return localIdxs;
};

const load = ({ localIdxs, change, prevStore, remoteIdxs, children, rebuild }) => {
  const item = change.value;
  const prev = prevStore[item.id];
  const shouldUpdateLocalIndex =
    (change.type === 'modified' && typeof item.parentIdx !== prev.parentIdx) ||
    change.type === 'added' ||
    change.type === 'removed';
  if (shouldUpdateLocalIndex) {
    localIdxs = collect(localIdxs, item, remoteIdxs, children[item.parent], { rebuild });
  }
  return localIdxs;
};

const setFn = (localIdxs) => (item) => assocMutating('index', localIdxs[item.id], item);

const setOnValue = (change, localIdxs) => {
    debugger;
  // could be done with lenses, but this seems less confusing
  change.value = [change.value]
    .map(clone)
    .map(setFn(localIdxs))[0];
  return change;
};

export const setOnValueFn = (localIdxs) => (change) => setOnValue(change, localIdxs);

export const loadFn = ({ store, remoteIdxs, children, prevStore }) => (localIdxs, change) =>
  load({ localIdxs, change, store, remoteIdxs, children, prevStore });

export const updateFn = ({ store, remoteIdxs, children, prevStore }) => (localIdxs, change) =>
  load({ localIdxs, change, store, remoteIdxs, children, prevStore, rebuild: true });

export const _ = {
  collect,
  load,
  setFn,
  setOnValue
};
