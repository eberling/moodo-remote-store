const by = (a, b, index) => index[a] - index[b];

const collect = (ids, item) => Object.assign(ids, { [item.id]: item.parentIdx });

export const load = (index, change) => {
  if (change.value.parentIdx !== undefined) {
    index = collect(index, change.value);
  }
  if (change.type === 'removed') delete index[change.value.id];
  return index;
};

export const ids = (index) => Object.keys(index);

export const byFn = (index) => (a, b) => by(a, b, index);

export const _ = {
  by,
  collect
};
