import { loader, updater, toChangeFn } from '../default-loader';

const addPermission = ({ doc, user, type, backend }) => {
  const newDoc = Object.assign(doc, { [type]: Object.assign(doc[type], { [user.id]: true }) });
  return backend.set(newDoc);
};

const removePermission = ({ doc, user, type, backend, specialProps }) => {
  const newDoc = Object.assign({}, doc);
  newDoc[type][user.id] = specialProps.remove();
  return backend.set(newDoc);
};

const loadWith = (data = [], api) => api._loader.load({ changes: data.map(toChangeFn('added')), api, prevStore: {} });

const loadWithFn = (api) => (data) => loadWith(data, api);

export default (api) => {
  const backend = api._backend;

  const loadWith = loadWithFn(api);

  const addOwner = (doc, user) => addPermission({ doc, user, type: 'owners', backend });
  const addWriter = (doc, user) => addPermission({ doc, user, type: 'writers', backend });
  const addReader = (doc, user) => addPermission({ doc, user, type: 'readers', backend });

  const removeOwner = (doc, user) =>
    removePermission({ doc, user, type: 'owners', backend, specialProps: api._specialProps });
  const removeWriter = (doc, user) =>
    removePermission({ doc, user, type: 'writers', backend, specialProps: api._specialProps });
  const removeReader = (doc, user) =>
    removePermission({ doc, user, type: 'readers', backend, specialProps: api._specialProps });

  const owners = (doc) => {};
  const writers = (doc) => {};
  const readers = (doc) => {};

  return Object.assign(api, {
    _loader: loader,
    _updater: updater,
    loadWith,
    addOwner,
    addWriter,
    addReader,
    removeOwner,
    removeWriter,
    removeReader,
    owners,
    readers,
    writers
  });
};
