import { loader, updater, toChangeFn } from '../items-loader';
import { locate } from '../split-index';
import * as remoteIndex from './remote-index';
import {
  toMap,
  whitelistPropsFn
} from './../functional';

const byParentThenByRemoteIndex = (a, b) => {
  const aParentBigger = a.parent > b.parent; // alpha
  const aParentEqual = a.parent === b.parent; // alpha
  const aRemoteIdxBigger = +a.parentIdx > +b.parentIdx;
  const aRemoteIdxEqual = +a.parentIdx === +b.parentIdx;
  const aLocalIdxBigger = +a._index > +b._index;

  if (aParentBigger) return -1;
  if (aParentEqual) {
    if (aRemoteIdxBigger) return -1;
    if (aRemoteIdxEqual && aLocalIdxBigger) return -1;
  }
  return 1;
};

// const moveItem = ({ item, backend, pair, parent, api }) => {
//   console.assert(!!item, 'item must exist');
//   const located = Object.assign(item, {
//     parent: parent || item.parent,
//     parentIdx: locate(pair.map(toMap(api._remoteIdxs)))
//   });
//   const moveProps = { id: true, parent: true, parentIdx: true };
//   const whitelist = whitelistPropsFn(moveProps);
//   const cleaned = whitelist(located);
//   const res = backend.update(cleaned);
//   api._updater.modifyItem(api, cleaned);
//   return res;
// };

// const moveItemFn = ({ item, backend, pair, parent, api }) => (pair, parent) =>
//   moveItem({ item, backend, pair, parent, api });

// const addLocatedItem = ({ item, backend, api, add, pair, parent }) => {
//   const located = Object.assign(item, {
//     parent: parent || item.parent,
//     parentIdx: locate(pair.map(toMap(api._remoteIdxs)))
//   });
//   return add(located);
// };

// const addItemFn = ({ item, backend, api, add }) => (pair, parent) =>
//   addLocatedItem({ item, backend, api, add, pair, parent });

// const loadWith = (data = [], api) => api._loader.load({ changes: data.map(toChangeFn('added')), api, prevStore: {} });

// const loadWithFn = (api) => (data) => loadWith(data, api);

export default (api) => {
  const oldAdd = api.add;

  const backend = api._backend;

//   const loadWith = loadWithFn(api);

  const all = () => api.allFn(byParentThenByRemoteIndex);

  const put = (item) => api.add(item);
    // chainLocation(addItemFn({ item, backend: api._backend, api: api, add: oldAdd }));

  const allFromRemote = () => backend.allFromRemote(remoteIndex.ids(api._remoteIdxs));

  const childIds = (parentId) => (api._children[parentId] || []).sort(remoteIndex.byFn(api._remoteIdxs)).concat([]);

  const getAt = (parentId, idx) => api.get(api.childIds(parentId)[idx]);

  const generateID = backend.generateID;

  return Object.assign(api, {
    _remoteIdxs: {},
    _localIdxs: {},
    _children: {},
    _loader: loader,
    _updater: updater,
    // loadWith,
    // add: null,
    all,
    put,
    allFromRemote,
    childIds,
    getAt
  });
};
