import { salt, key, encrypt, decrypt } from './encrypt';

const noop = (foo) => foo;

export const encrypter = (config) => {
  return {
    encrypt,
    decrypt
  };
};

export const noEncrypter = () => ({
  encrypt: noop,
  decrypt: noop
});
