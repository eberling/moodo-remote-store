// https://stackoverflow.com/a/27865285/4048117
const precision = (a) =>  {
  if (!isFinite(a)) return 0;
  let e = 1;
  let p = 0;
  while (Math.round(a * e) / e !== a) { e *= 10; p++; }
  return p;
};

const roundF = (num, digits) => {
  const mult = Math.pow(10, digits);
  return Math.round(num * mult) / mult;
};

const toFixedDown = (num, digits) => {
  const re = new RegExp("([-+]?\\d+\\.\\d{" + digits + "})(\\d)");
  const m = num.toString().match(re);
  return m ? parseFloat(m[1]) : num.valueOf();
};

const atP = (num, digits) => (roundF(num, digits) + '').split('').reverse()[0];

// avoid lengthening the float when finding halfway points
// so split(1, 2) -> 1.5, split(1.5, 2) -> 1.7, split(1.9, 2) -> 1.95
const splitIndex = (aVal, bVal) => {
  // TODO assert a < b, a & b not Infinite

  const aP = precision(aVal); // 0.1 => 1
  const bP = precision(bVal); // 0.155 => 3
  const minP = Math.min(aP, bP);
  const maxP = Math.max(aP, bP);
  const half = roundF((aVal + bVal) / 2, maxP + 1);

  let currP = minP;
  let stopAt = 100; // infinite loop break
  while ((atP(aVal, currP) === atP(half, currP) || atP(half, currP) === atP(bVal, currP)) && stopAt > 0) {
    currP++;
    stopAt--;
  }

  return roundF(half, currP);
};

// split pair of indexes
export const locate = ([aIdx, bIdx]) => {
  let split = 0;
  if (typeof aIdx === 'number' && typeof bIdx === 'number') { split = splitIndex(aIdx, bIdx); }
  else if (typeof aIdx === 'number') { split = aIdx + 1; }
  else if (typeof bIdx === 'number') { split = bIdx - 1; }

  return split;
};

export const _ = {
  precision,
  roundF,
  toFixedDown,
  atP,
  splitIndex
};
