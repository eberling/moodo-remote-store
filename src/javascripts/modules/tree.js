let dataCache;

const toChangeInternal = (snapshot, type) => {
    let path = snapshot.ref.path.slice(2),
        val = snapshot.val(),
        value = {},
        obj = value,
        cached = dataCache;

    for (let i = 0; i < path.length; i ++)
    {
        let p = path[i];
        obj[p] = {};
        obj = obj[p];

        if (type !== 'removed')
        {
            if (!cached[p])
            {
                cached[p] = {};
            }
            if (cached)
            {
                cached = cached[p];
            }
        }
    }

    Object.keys(val).forEach(key => {
        obj[key] = val[key];

        if (type !== 'removed')
        {
            cached[key] = val[key];
        }
        else if (cached)
        {
            delete cached[key];
        }
    });

    return { type, value };
};

export const create = ({ db, path }) => {

  const firebase = db.firebase;
  const session = db.session;

  let isLoaded = false,
    ignoreLocalChange = false;

  const get = (p) => {
      let arr = p.split('/'),
        obj = dataCache;

    arr.forEach((key) => {
        if (!obj[key])
        {
            obj[key] = {};
        }
        obj = obj[key];
    });

    return obj;
  };

  const set = (p, value) => {
      let arr = p.split('/'),
        obj = dataCache,
        key = arr[arr.length - 1];

    for (let i = 0; i < arr.length - 1; i ++)
    {
        if (!obj)
        {
            return false;
        }
        if (!obj[arr[i]])
        {
            obj[arr[i]] = {};
        }
        obj = obj[arr[i]];
    }

    obj[key] = value;
    ignoreLocalChange = true;

    let ref = session.ref(`${path}/${p}`),
        ret = ref.set(value);

    ignoreLocalChange = false;
    return ret;
  };

  const generateId = () => session.ref(path).push().key;

  const remove = (p) => {
    return session.ref(`${path}/${p}`).remove();
  };

  const onLoad = (cb) => {
      session.ref(path)
          // TODO this can potentially resolve before our callbacks are attached, but it doesn't seem to
          // it might work better to start typeening only after the first callback is attached
          .once('value', (snapshot) =>
          {
              if (isLoaded) return;
              isLoaded = true;
              const val = snapshot.val() || {};
              dataCache = val;

              cb(val);
          }, (error) =>
          {
              console.log('Firebase error', error);
          });
  };

  const onUpdate = (cb) => {
    // logic based on https://stackoverflow.com/a/27995609
    // TODO offUpdate removes these
    session.ref(path)
      .on('child_added', (snapshot) => {
        if (!isLoaded || ignoreLocalChange) return;

        const change = toChangeInternal(snapshot, 'added');
        cb(change);
      }, (error) => {
        console.log('Firebase error', error);
      });
    session.ref(path)
      .on('child_changed', (snapshot) => {
        if (!isLoaded || ignoreLocalChange) return;

        const change = toChangeInternal(snapshot, 'modified');
        cb(change);
      }, (error) => {
        console.log('Firebase error', error);
      });
    session.ref(path)
      .on('child_removed', (snapshot) => {
        if (!isLoaded || ignoreLocalChange) return;

        const changes = toChangeInternal(snapshot, 'removed');
        cb(changes);
      }, (error) => {
        console.log('Firebase error', error);
      });
  };

  return ({
    set,
    get,
    remove,
    onLoad,
    onUpdate,
    generateId,
  });
};

export const _ = {
  toChangeInternal
};
