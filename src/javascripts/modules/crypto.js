import { salt, key, encrypt, decrypt } from './encrypt';

const DEFAULT_SALT = 'afolaw8klslbsahgbeoiya4pifoqlncduzvxgze60lvk17sevauroatsxm8qqia';

const globalKey = {
    alg: 'A256CTR',
    ext: true,
    k: 'TB7ZRnU1d_AhFpMYauQAVq_7ny_ofFnmE0Hz0zpTWJ4',
    key_ops: [ 'encrypt', 'decrypt' ],
    kty: 'oct'
};

const createKeyInfo = ({ name, password, sentinel, salt, now }) => {
  const hasKey = key(password, salt);
  const hasSentinel = hasKey
    .then((key) => encrypt(sentinel, key, salt));

  return Promise.all([hasKey, hasSentinel])
    .then(([key, encryptedSentinel]) => ({
      name,
      key,
      salt,
      hasPassword: !!password,
      sentinel: encryptedSentinel,
      createdAt: now
    }));
};

const isPasswordValid = (verifyPassword, controlKey, salt) => {
  const hasControlEncryption = encrypt('TEST', controlKey, salt);

  const hasVerifyKey = key(verifyPassword, salt);
  const hasVerifyEncryption = hasVerifyKey
    .then((verifyKey) => encrypt('TEST', verifyKey, salt));

  return Promise.all([hasControlEncryption, hasVerifyEncryption])
    .then(([controlEncryption, verifyEncryption]) => {
      return controlEncryption === verifyEncryption;
    });
};

const importKeys = (keys, newKeys = []) => {
    newKeys = newKeys.concat([{ name: 'global', value: globalKey }]);
    return Promise.all(newKeys.map(({ name, value }) => {
        return crypto.subtle.importKey('jwk', value, 'AES-CTR', true, [ 'encrypt', 'decrypt' ]).then((imported) => {
            keys[name] = {
                key: imported,
                salt: DEFAULT_SALT
            };
            return imported;
        });
    }));
};

const exportKey = (keys, name) => {
    if (keys[name])
    {
        return crypto.subtle.exportKey('jwk', keys[name].key).then((value) => {
            return { name, value };
        });
    }
    return Promise.reject();
};

const setKey = ({ keys, name, password, sentinel, salt, now }) => {
  return createKeyInfo({ name, password, sentinel, salt, now })
    .then(keyInfo => {
      keys[name] = keyInfo;
      return keyInfo;
    });
};

const updateKey = ({ keys, name, newPassword, newSentinel, newSalt, now }) => {
  const oldKeyInfo = keys[name];

  const hasNewKeyInfo = createKeyInfo({ name, newPassword, newSentinel, newSalt, now });
  return hasNewKeyInfo
    .then((newKeyInfo) => {
      keys[name] = newKeyInfo;
      return { oldKeyInfo, newKeyInfo };
    });
};

const deleteKey = ({ keys, name }) => {
  const keyInfo = keys[name];
  delete keys[name];
  return { oldKeyInfo: keyInfo };
};

const canDecrypt = ({ source, target, key }) => {
  return decrypt(source, key.key, key.salt, key.salt)
    .then(text => text === target);
};

const findDecryptKeys = ({ source, target, keys }) => {
  const hasDecrypted = Object.values(keys)
    .map((key) => {
      return decrypt(source, key.key, key.salt, key.salt)
        .then(text => ({ name: key, text }));
    });
  return Promise.all(hasDecrypted)
    .then(decrypted => decrypted.filter(found => found.text === target).map(found => found.name));
};

const validatePassword = (keys, name, verifyPassword) => {
  const oldKeyInfo = keys[name];

  let hasValidPassword = Promise.resolve(true);
  if (oldKeyInfo.hasPassword) {
    hasValidPassword = isPasswordValid(verifyPassword, oldKeyInfo.key, oldKeyInfo.salt);
  }
  return hasValidPassword;
};

// TODO files for the custom errors. Babel can't extend builtin Error, so we're using the old approach
export function InvalidArgumentError(message) {
    this.message = message;
    // Use V8's native method if available, otherwise fallback
    if ("captureStackTrace" in Error)
        Error.captureStackTrace(this, InvalidArgumentError);
    else
        this.stack = (new Error()).stack;
}
InvalidArgumentError.prototype = Object.create(Error.prototype);
InvalidArgumentError.prototype.name = "InvalidArgumentError";
InvalidArgumentError.prototype.constructor = InvalidArgumentError;

export function InvalidPasswordError(message) {
    this.message = message;
    // Use V8's native method if available, otherwise fallback
    if ("captureStackTrace" in Error)
        Error.captureStackTrace(this, InvalidPasswordError);
    else
        this.stack = (new Error()).stack;
}
InvalidPasswordError.prototype = Object.create(InvalidArgumentError.prototype);
InvalidPasswordError.prototype.name = "InvalidPasswordError";
InvalidPasswordError.prototype.constructor = InvalidPasswordError;

// for clients to check

export const create = ({ keys = [], sentinel = 'sentinel', system = {} } = {}) => {
  const api = {};

  const state = {
    keys: importKeys({}, keys),
    sentinel
  };

  api.importKeys = (keys) => {
      return importKeys(state.keys, keys);
  };

  api.exportKey = (name) => {
      return exportKey(state.keys, name);
  };

  // crypto.create({ name: 'my-key' })
  // crypto.create({ name: 'my-key', password: 'my-password' })
  api.add = ({ name, password }) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`));
    if (name in state.keys) return Promise.reject(new InvalidArgumentError(`${name} exists. Use 'update' to change existing keys.`));

    return setKey({
      keys: state.keys,
      name,
      password,
      sentinel: state.sentinel,
      salt: DEFAULT_SALT,
      now: (new Date())
    });
  };

  api.salt = salt;

  // crypto.update({ name: 'my-key' }) -- original key was passwordless
  // crypto.update({ name: 'my-key', newPassword: 'my-new-password' }) -- originial key was passwordless
  // crypto.update({ name: 'my-key', password: 'my-password' }) -- new key is passwordless
  api.update = ({ name, password, newPassword }) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`));
    if (!(name in state.keys)) return Promise.reject(new InvalidArgumentError(`${name} does not exist. Use 'create' to add new keys.`));

    return validatePassword(state.keys, name, password)
      .then((isPasswordValid) => {
        if (!isPasswordValid) throw new InvalidPasswordError();

        return updateKey({
          keys: state.keys,
          name,
          newPassword,
          newSalt: DEFAULT_SALT,
          now: (new Date())
        });
      });
  };

  // like update, but tramples over existing stuff, doesn't verify much, exposes lesser-used options
  api.forceUpdate = ({ name, newPassword, newSentinel, generateSalt }) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`));

    return updateKey({
      keys: state.keys,
      name,
      newPassword,
      newSentinel,
      newSalt: generateSalt ? salt() : DEFAULT_SALT,
      now: (new Date())
    });
  };

  // crypto.delete('my-key') -- for passwordless keys
  // crypto.delete('my-key', 'my-password')
  api.delete = ({ name, password }) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`));
    if (!(name in state.keys)) return Promise.reject(new InvalidArgumentError(`Key not found: ${name}`));

    return validatePassword(state.keys, name, password)
      .then((isPasswordValid) => {
        if (!isPasswordValid) throw new InvalidPasswordError();

        deleteKey({ keys: state.keys, name });
      });
  };

  api.forceDelete = ({ name }) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`));
    if (!(name in state.keys)) return Promise.reject(new InvalidArgumentError(`Key not found: ${name}`));

    return deleteKey({ keys: state.keys, name });
  };

  // crypto.verify('my-key', 'my-password')
  api.verify = ({ name, password }) => {
    return validatePassword(state.keys, name, password);
  };

  // crypto.delete('my-key')
  api.exists = (name) => !!(name in state.keys);

  // crypto.canEncrypt('my-key', 'XDXDXDXD')
  // crypto.canEncrypt('my-key', 'XDXDXDXD', 'custom-sentinel')
  api.canEncrypt = (name, encryptedSource, target = state.sentinel) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`, name));
    if (!(name in state.keys)) return Promise.reject(new InvalidArgumentError(`Key not found: ${name}`));

    return canDecrypt({
      source: encryptedSource,
      target,
      key: state.keys[name]
    });
  };

  api.canDecrypt = api.canEncrypt;

  // crypto.findBySentinel('XDXDXDXD')
  // crypto.findBySentinel('XDXDXDXD', 'custom-sentinel')
  api.findBySentinel = (encryptedSource, target = state.sentinel) => {
    if (typeof encryptedSource !== 'string') return Promise.reject(new InvalidArgumentError(`encryptedSource must be a string. Saw: ${encryptedSource}`));
    if (typeof target !== 'string') return Promise.reject(new InvalidArgumentError(`Target must be a string. Saw: ${target}`));

    return findDecryptKeys({
      source: encryptedSource,
      target,
      keys: state.keys
    });
  };

  api.encryptText = (name, text) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`, name));
    // TODO discuss: should encrypt accept non-string as text
    if (typeof text !== 'string') return Promise.reject(new InvalidArgumentError(`Text must be a string. Saw: ${text}`, text));
    if (!(name in state.keys)) return Promise.reject(new InvalidArgumentError(`Key not found: ${name}`));

    const key = state.keys[name];
    return encrypt(text, key.key, key.salt);
  };

  api.decryptText = (name, text) => {
    if (typeof name !== 'string') return Promise.reject(new InvalidArgumentError(`Name must be a string. Saw: ${name}`, name));
    // TODO discuss: should encrypt accept non-string as text
    if (typeof text !== 'string') return Promise.reject(new InvalidArgumentError(`Text must be a string. Saw: ${text}`, text));
    if (!(name in state.keys)) return Promise.reject(new InvalidArgumentError(`Key not found: ${name}`));

    const key = state.keys[name];
    return decrypt(text, key.key, key.salt, key.salt);
  };

  api.export = () => {
    return Object.values(state.keys);
  };

  if (system.__DEV__) {
    api.__ = state;
  }

  return api;
};

export const _ = {
  createKeyInfo,
  isPasswordValid,
  importKeys,
  setKey,
  updateKey,
  deleteKey,
  canDecrypt,
  findDecryptKeys,
  validatePassword,
  InvalidArgumentError,
  InvalidPasswordError
};
