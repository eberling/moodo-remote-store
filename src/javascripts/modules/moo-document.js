import * as tree from './tree';

export default ({ db, docID, uid, userDocuments } = {}) =>
{
    let database = db.session,
        treeApi = tree.create({ db, path: `documents/${docID}` }),
        api = {};

    api.onLoad = (cb) => {
        treeApi.onLoad((value) => {
            cb(value);
        });
    };

    api.onUpdate = treeApi.onUpdate;

    api.create = (doc, items) => {
        let save = Object.assign({}, doc);
        delete save.id;

        var val = {
            [`/documents/${docID}`]: save,
            [`/userdocuments/${uid}/${docID}`]: { d: save.p.d, t: save.p.t }
        };

        if (items)
        {
            for (var i = 0; i < items.length; i ++)
            {
                let item = items[i],
                    itemID = item.id;

                delete item.id;

                val[`/items/${docID}/${itemID}`] = item;
            }
        }

        database.ref().update(val);

        // treeApi.set('', doc);
        // userDocuments.add()
    };

    api.deleteFully = (cb) => {
        var val = {
            [`/items/${docID}`]: null,
            [`/userdocuments/${uid}/${docID}`]: null,
            [`/documents/${docID}`]: null,
        };

        database.ref().update(val);
    };

    api.update = (doc, ) => {
        let save = Object.assign({}, doc);
        delete save.id;

        var val = {
            [`/documents/${docID}`]: save,
        };

        if (save.p.d || save.p.t)
        {
            if (save.p.d)
            {
                val[`/userdocuments/${uid}/${docID}/p/d`] = save.p.d;
            }
            if (save.p.t)
            {
                val[`/userdocuments/${uid}/${docID}/p/t`] = save.p.t;
            }
        }
        database.ref().update(val);
    };

    // api.getSetting = (setting) => {
    //     return treeApi.get('s/' + setting);
    // };
    // api.setSetting = (setting, value) => {
    //     return treeApi.set('s/' + setting, value);
    // };
    // api.removeSetting = (setting) => {
    //     return treeApi.remove('s/' + setting);
    // };

    // api.getProperty = (property) => {
    //     return treeApi.get('p/' + property);
    // };
    // api.setProperty = (property, value) => {
    //     return treeApi.set('p/' + property, value);
    // };

    return api;
};
