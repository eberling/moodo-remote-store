import {
  indexBy,
  jsonDiff,
} from './functional';

const toDiff = (type, prev, curr) => {
  if (type === 'removed' || type === 'added') return { type, value: curr };
  return { type, value: jsonDiff(prev, curr) };
};

const toDiffFn = (store) => (change) =>
  toDiff(change.type, store[change.value.id], change.value);

const toChange = (item, type) => ({ type, value: item });

export const toChangeFn = (type) => (item) => toChange(item, type);

const setDiffOnValueFn = (prevStore, setOnValueFn) => (change) =>
  [change]
    .map(toDiffFn(prevStore))[0];

const loadStore = (store, change) => {
  const item = change.value;
  store = indexBy('id')(store, item);
  if (change.type === 'removed') delete store[item.id];
  return store;
};

const normalizeLoadChanges = ({ changes }) => {
  return changes
    .map((c) => c.value);
};

const normalizeUpdateChanges = ({ changes, prevStore }) => {
  return changes
    .map(setDiffOnValueFn(prevStore));
};

const reset = (api) => {
  api._store = {};
  return api;
};

const load = ({ changes, type = 'load', prevStore, api }) => {
  api._store = changes.reduce(loadStore, api._store);
  return api;
};

const localUpdate = ({ item, api, changeType }) => {
  const prevStore = Object.assign({}, api._store);
  const change = toChange(item, changeType);
  load({ changes: [change], prevStore, api });
};

const localUpdateReducer = (changeType) => (api, item) =>
  localUpdate({ item, api, changeType });

// minimalist interface to do keyword-based polymorphism
export const updater = {
  load: ({ changes, api, prevStore }) =>
    load({ api, type: 'update', changes, prevStore }),
  normalizeChanges: normalizeUpdateChanges,
  addItem: localUpdateReducer('added'),
  removeItem: localUpdateReducer('removed'),
  modifyItem: localUpdateReducer('modified')
};

export const loader = {
  load: ({ changes, api, prevStore }) => load({ changes, api: reset(api), prevStore }),
  normalizeChanges: normalizeLoadChanges
};

export const _ = {
  toDiff,
  toChange,
  setDiffOnValueFn,
  loadStore,
  normalizeLoadChanges,
  normalizeUpdateChanges,
  load,
  localUpdate
};
