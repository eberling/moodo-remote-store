import { hook, unhook, invoke } from './hooks';

const random = ([min, max]) => min + Math.random() * (max - min);

const randomInt = ([min, max]) =>
  Math.floor(random([min, max]) + 1);

export const create = ({ db, type = 'items' }) => {
  const db = { [type]: {} };
  const callbacks = [];

  const generateId = () => Math.floor(1e10 + (Math.random() * 1e11 - 1e10 - 1));

  const getFromRemote = (id) => Promise.resolve(db[type][id]);

  const allFromRemote = (ids) =>
    Promise.all(ids.map((id) => getFromRemote({ id, db, type })));

  const add = (item) => {
    console.assert(!(item.id in db[type]), 'Item exists');
    const id = item.id || randomInt([1e10, 1e11 - 1]);
    db[type][id] = item;
    const copy = Object.assign({}, item);
    const promise = Promise.resolve(copy);
    setTimeout(() => {
      callbacks.forEach({ type: 'added', value: copy });
    }, 0);
    return {
      data: copy,
      response: promise
    };
  };

  const set = (item) => {
    db[type][item.id] = item;
    const copy = Object.assign({}, item);
    setTimeout(() => {
      callbacks.forEach({ type: 'modified', value: copy });
    }, 0);
    Promise.resolve(copy);
  };

  const merge = (item) => {
    console.assert(!!db[type][item.id], 'Item does not exist');
    const result = Object.assign(db[type][item.id], item);
    const copy = Object.assign({}, item);
    setTimeout(() => {
      callbacks.forEach({ type: 'modified', value: copy });
    }, 0);
    Promise.resolve(result);
  };

  const update = (item) => merge;

  const remove = (item) => {
    delete db[type][item.id];
    const copy = Object.assign({}, item);
    setTimeout(() => {
      callbacks.forEach({ type: 'removed', value: copy });
    }, 0);
    return true;
  };

  const onUpdate = (cb) => hook(callbacks, cb);

  const offUpdate = (cb) => unhook(callbacks, cb);

  return ({
    getFromRemote,
    allFromRemote,
    generateId,
    add,
    set,
    merge,
    update,
    remove,
    onUpdate,
    offUpdate
  });
};
