const remoteId = (change) => {
  const segs = change.doc._key.path.segments;
  return segs[segs.length - 1];
};

const toItem = (change) => {
  const doc = change.doc._document.value();
  const id = remoteId(change);
  if (id !== doc.id) doc._id = id;
  return doc;
};

const toChangeInternal = (change) => ({
  type: change.type,
  value: toItem(change)
});

export const init = (config) => {
  const firebase = config.firebase;
  const session = firebase.firestore();

  if (!config.__DEV__) {
    firebase.firestore.setLogLevel('silent');
  }

  session.settings({ timestampsInSnapshots: true });

  return Promise.resolve({})
    // Initialize Cloud Firestore through firebase
    .then(() => {
      // TODO can i reuse the existing firestore?
      return { firebase, session: firebase.firestore() };
    }).catch((error) => {
        console.log('Firestore connect error', error);
    });
    // if (err.code == 'failed-precondition') {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
    //    } else if (err.code == 'unimplemented') {
      // The current browser does not support all of the
      // features required to enable persistence
      // ...

  // firebase.auth()
  //   .onAuthStateChanged((user) => Object.assign(api, { _user: user }));
};

export const create = ({ db, path = 'items' }) => {

  const firebase = db.firebase;
  const session = db.session;

  const getFromRemote = (id) => {
    const docRef = session.collection(path).doc(id);

    // Get a document, forcing the SDK to fetch from the offline cache.
    return docRef.get({ source: 'cache' }).then((doc) => {
      // Document was found in the cache. If no cached document exists,
      // an error will be returned to the 'catch' block below.
      return doc.data();
    }).catch((error) => {
      return docRef.get({ source: 'server' })
        .then((doc) => doc.data());
    });
  };

  const allFromRemote = (ids) =>
    Promise.all(ids.map((id) => getFromRemote({ id, db, path })));

  const add = (item) => {
    const ref = session.collection(path).doc();
    // Firestore gave us an id, so we save it as a property
    const copy = Object.assign({}, item, { id: ref.id });
    const promise = ref.set(copy);
    return {
      data: copy,
      response: promise
    };
  };

  const generateId = () => session.collection(path).doc().id;

  const set = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Set requires document id.');

    const ref = session.collection(path).doc(item.id);
    return ref.set(item);
  };

  const merge = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Merge requires document id.');

    const ref = session.collection(path).doc(item.id);
    const copy = Object.assign({}, item);
    return ref.set(item, { merge: true });
  };

  const update = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Update requires document id.');

    const ref = session.collection(path).doc(item.id);
    const copy = Object.assign({}, item);
    return ref.update(copy);
  };

  const remove = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Remove requires document id.');

    return session.collection(path).doc(item.id).delete();
  };

  const onUpdate = (cb) => {
    return session.collection(path)
      // TODO this can potentially resolve before our callbacks are attached, but it doesn't seem to
      // it might work better to start typeening only after the first callback is attached
      .onSnapshot((snapshot) => {
        const changes = snapshot.docChanges()
          .map(toChangeInternal);
        cb(changes, snapshot);
      }, (error) => {
        console.log('Firestore error', error);
      });
  };

  const specialProps = {
    now: () => firebase.firestore.FieldValue.serverTimestamp(),
    remove: () => firebase.firestore.FieldValue.delete()
  };

  return ({
    getFromRemote,
    allFromRemote,
    add,
    set,
    merge,
    update,
    remove,
    onUpdate,
    generateId,
    specialProps
  });
};

export const _ = {
  toItem,
  remoteId,
  toChangeInternal
};
