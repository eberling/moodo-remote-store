export const init = (config) => {
  const firebase = config.firebase;
  return Promise.resolve({})
    .then(() => {
      return { firebase, session: firebase.database() };
    });
};

export const onOnlineChange = (db, cb) =>
{
    db.session.ref('.info/connected').on('value', (snap) =>
    {
        cb(snap.val() === true);
    });
};

export const serverTime = (db) => db.firebase.database.ServerValue.TIMESTAMP;
export const generateId = (db) => db.firebase.database().ref().push().key;

const toChangeInternal = (id, item, type) => {
    item.id = id;
    return {
        type,
        value: item
    };
};

const toChangeInternalFn = (snapshotVal, type) => (id) => toChangeInternal(id, snapshotVal[id], type);

export const create = ({ db, path = 'items', orderBy, startAt }) => {

  const firebase = db.firebase;
  const session = db.session;

  const getFromRemote = (id) => {
    const docRef = session.ref(`${path}/${id}`);

    return docRef.once('value').then((snap) => {
      return snap.val();
    });
  };

  const allFromRemote = (ids) =>
    Promise.all(ids.map((id) => getFromRemote({ id, db, path })));

  const add = (item) => {
    const ref = session.ref(path).push();
    // Firestore gave us an id, so we save it as a property
    const copy = Object.assign({}, item, { id: ref.key });
    const promise = session.ref(`${path}/${copy.id}`).set(copy);
    return {
      data: copy,
      response: promise
    };
  };


  const set = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Set requires document id.');

    const ref = session.ref(`${path}/${item.id}`);
    delete item.id;
    return ref.set(item);
  };

  const merge = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Merge requires document id.');

    const ref = session.ref(`${path}/${item.id}`);
    const copy = Object.assign({}, item);
    delete item.id;
    return ref.set(item, { merge: true });
  };

  const update = (item, modifyItem) => {
    // console.assert(item.id && typeof item.id === 'string', 'Update requires document id.');

    const ref = session.ref(`${path}/${item.id}`);
    const copy = Object.assign({}, item);
    delete copy.id;

    return new Promise((success) => {
        let val;
        let onValue = (value) => {
            val = value.val();
            if (val)
            {
                modifyItem(Object.assign({ id: item.id }, val));
                ref.off('value', onValue);
            }
        };
        ref.on('value', onValue);
        ref.update(copy).then(() => {
            if (__DEV__ && !val) { console.log('no val', item, copy); debugger; }
            success(Object.assign({ id: item.id }, val));
        });
    });
  };

  const remove = (item) => {
    // console.assert(item.id && typeof item.id === 'string', 'Remove requires document id.');

    return session.ref(`${path}/${item.id}`).remove();
  };

  const onUpdate = (cb) => {

    // logic based on https://stackoverflow.com/a/27995609
    // TODO offUpdate removes these
    let isLoaded = false;

    let ref = session.ref(path);

    if (orderBy && startAt)
    {
        ref = ref.orderByChild(orderBy).startAt(startAt);
    }

      // TODO this can potentially resolve before our callbacks are attached, but it doesn't seem to
      // it might work better to start typeening only after the first callback is attached
    ref.once('value', (snapshot) => {
        if (isLoaded) return;
        isLoaded = true;
        let snapshotVal = snapshot.val();
        const changes = Object.keys(snapshotVal || {})
          .map(toChangeInternalFn(snapshotVal, 'added'));
        cb(changes);
      }, (error) => {
        console.log('Firebase error', error);
      });

    ref.on('child_added', (snapshot) => {
        if (!isLoaded) return;

        const changes = [toChangeInternal(snapshot.key, snapshot.val(), 'added')];
        cb(changes);
      }, (error) => {
        console.log('Firebase error', error);
      });

    ref.on('child_changed', (snapshot) => {
        if (!isLoaded) return;


          const changes = [toChangeInternal(snapshot.key, snapshot.val(), 'modified')];
        cb(changes);
      }, (error) => {
        console.log('Firebase error', error);
      });

    ref.on('child_removed', (snapshot) => {
        if (!isLoaded) return;


          const changes = [toChangeInternal(snapshot.key, snapshot.val(), 'removed')];
        cb(changes);
      }, (error) => {
        console.log('Firebase error', error);
      });
  };

  return ({
    getFromRemote,
    allFromRemote,
    add,
    set,
    merge,
    update,
    remove,
    onUpdate
  });
};

export const _ = {
  toChangeInternal
};
