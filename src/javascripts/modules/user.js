import * as tree from './tree';

export default ({ db, uid } = {}) =>
{
    let treeApi = tree.create({ db, path: `users/${uid}` }),
        api = {};

    api.onLoad = (cb) => {
        treeApi.onLoad((value) => {
            cb({ properties: value.p, settings: value.s });
        });
    };
    api.onUpdate = treeApi.onUpdate;
    api.getSetting = (setting) => {
        return treeApi.get('s/' + setting);
    };
    api.setSetting = (setting, value) => {
        return treeApi.set('s/' + setting, value);
    };
    api.removeSetting = (setting) => {
        return treeApi.remove('s/' + setting);
    };

    api.getProperty = (property) => {
        return treeApi.get('p/' + property);
    };
    api.setProperty = (property, value) => {
        return treeApi.set('p/' + property, value);
    };

    api.deleteFully = (cb) => {
        var val = {
            [`/users/${uid}`]: null,
        };

        db.session.ref().update(val);
    };

    return api;
};
