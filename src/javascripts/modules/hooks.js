/*jshint module:true*/

/*
 * Observer pattern
 */

export const hook = (list, f, returnVal) => {
  list.push(f);
  return returnVal;
};

export const hookOnce = (list, f, returnVal) => {
  if (list.includes(f)) return returnVal;
  list.push(f);
  return returnVal;
};

export const hookFn = (list, returnVal) => (f) => hook(list, f, returnVal);

export const hookOnceFn = (list, returnVal) => (f) => hookOnce(list, f, returnVal);

export const unhook = (list, f, returnVal) => {
  const idx = list.indexOf(f);
  if (idx > -1) {
    list.splice(idx, 1);
  }
  return returnVal;
};

export const unhookFn = (list, returnVal) => (f) => unhook(list, f, returnVal);

export const invoke = (list, args, returnVal) => {
  // an error in the execution stack of called functions should not stop the invocation
  // setTimeout forks a new event loop to isloate those errors
  for (let cb of list) setTimeout(() => cb(...args), 0);
  return returnVal;
};

export const invokeSync = (list, args, returnVal) => {
  for (let cb of list) cb(...args);
  return returnVal;
};

export const invokeFn = (list, returnVal) => (...args) => invoke(list, args, returnVal);

export const invokeSyncFn = (list, returnVal) => (...args) => invokeSync(list, args, returnVal);
