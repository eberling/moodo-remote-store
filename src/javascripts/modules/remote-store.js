// import firebase from '@firebase/app';
// import '@firebase/firestore';
import * as items from './items';
import * as backend from './backends/firebase';
import mooItems from './mixins/moo-items';
import mooDocument from './moo-document';
import user from './user';

export const create = ({ config, initialData } = {}) => {
  return backend.init(config).then(db => {
    const api = {};
    initialData = initialData || {
        items: [],
        documents: []
    };

    const uid = config.uid;

    api.user = user({ db, backend, uid: uid });

    // api.userDocuments = items.create({ db, backend, path: `userDocuments/${uid}` });

    // api.test = mooItems(items.create({ db, backend, path: 'test' }));

    // api.documents = mooDocuments(items.create({ db, backend, path: 'documents' }))
    //   .loadWith(initialData.documents);

    const docs = {};

    let userDocuments = items.create({ db, backend, path: `userdocuments/${uid}` });

    api.loadDocuments = (cb) => {
        userDocuments.onLoad(({ responses }) => {
            cb(responses);
        });
    };

    api.document = (docID, dateModified) => {
      if (!(docID in docs)) {
        const newDoc = mooDocument({ db, docID, uid, userDocuments });
        //   .loadWith(initialData.documents.filter(isItemInDocFn(doc)));
        const newDocItems = mooItems(items.create({ db, backend, path: `items/${docID}`, orderBy: 'dateModified', startAt: dateModified }));
        //   .loadWith(initialData.items.filter(isItemInDocFn(doc)));

        docs[docID] = {
            info: newDoc,
            items: newDocItems,
        };
      }
      return docs[docID];
    };

    api.addDocument = (doc, items) => {
        const docID = doc.id;
      if (!(docID in docs)) {
        const newDoc = mooDocument({ db, docID, uid });
        newDoc.create(doc, items);
        //   .loadWith(initialData.documents.filter(isItemInDocFn(doc)));
        // const newDocItems = mooItems(items.create({ db, backend, path: `items/${docID}` })).loadWith([]);
        // //   .loadWith(initialData.items.filter(isItemInDocFn(doc)));
        // docs[docID] = {
        //     info: newDoc,
        //     items: newDocItems
        // };
      }
    };

    api.forgetDocument = (docID) => {
      const docToUnload = docs[docID];
      delete docs[docID];

      // TODO make a .reset
      docToUnload._callbacks = { update: [], load: [], updateDebug: [], loadDebug: [] };
      return true;
    };

    api.onOnlineChange = (cb) => backend.onOnlineChange(db, cb);
    api.serverTime = backend.serverTime(db);
    api.generateId = backend.generateId.bind(backend, db);

    return api;
  });
};
