import { loader, updater, toChangeFn } from './default-loader';
import { hook, unhook, invoke } from './hooks';
import {
  compose,
  pluckFn,
  mark,
  blacklistPropsFn
} from './functional';

let _isUpdating = false; // Use this to prevent onSnapshot from being called while updating

const chainPromisable = (tuple) => ({
  data: () => tuple.data,
  response: () => tuple.response
});

export const onSnapshot = (changes, snapshot, api) => {
  if (_isUpdating)
    return;

  const prevStore = Object.assign({}, api._store);
  const type = api._loadingState;
  const processor = type === 'load' ? api._loader : api._updater;

  const isLoad = type === 'load';

  // Filter out local changes
  if (!isLoad)
  {
    // TODOFIRE: Make this check other fields too? Or is dateModified enough?
    // Note: 'j' is the translated dateModified field. See FieldTranslator.js
    changes = changes.filter(({ value }) => !prevStore[value.id] || value.j !== prevStore[value.id].j);
  }

  // updates can occur if writes are sent before we get the load snapshot
  // queue updates to ensure load always occurs first (for api consumers)
  // load = first with !hasPendingWrites
  // when offline and the cache is not primed, onSnapshot will only fire for local edits
  // when offline and the cache is primed and there is an exiting pending write on pageload,
  //   that write triggers a snapshot and then
  //   a separate snapshot will fire for load
  if (isLoad) {
    api._loadingState = 'update';
    const isLoadedAndCanReplayEarlyEdits = type === 'load' && api._earlyEdits.length;
    if (isLoadedAndCanReplayEarlyEdits) {
      api._earlyEdits.forEach((edit) => {
        invoke(api._callbacks.update, [edit]);
      });
      api._earlyEdits = [];
    }
  }

  if (api._loadingState === 'update') {
      if (isLoad || changes.length > 0)
      {
          api = processor.load({ api, changes, prevStore });

          const responses = processor.normalizeChanges({ api, changes, prevStore });

          invoke(api._callbacks[type], [{ responses, snapshot }]);
          invoke(api._callbacks[`${type}Debug`], [{ responses, changes, snapshot, api }]);
      }
  } else {
    // queue the local change
    // note: we assume snapshot reference is new, not shared between calls
    api._earlyEdits.push({ changes, snapshot, api });
  }

  return api;
};

export const onSnapshotFn = (api) => (changes, snapshot) =>
  onSnapshot(changes, snapshot, api);

const all = ({ store, sort }) =>
  Object.values(store).map((item) => Object.assign({}, item)).sort(sort);

const get = ({ id, store }) => store[id] ? Object.assign({}, store[id]) : undefined;

// const addItem = ({ item, backend, api }) => {
//     debugger;

//   console.assert(!!item, 'item must exist');
//   const blacklist = blacklistPropsFn(api._ignore);
//   const upsert = item.id ? backend.set : backend.add;
//   const cleaned = blacklist(item);
//   api._updater.addItem(api, cleaned);

//     _isUpdating = true;
//     let ret = upsert(cleaned);
//     _isUpdating = false;

//   return chainPromisable(ret);
// };

// const saveItem = ({ item, backend, api }) => {
//     debugger;
//   console.assert(!!item, 'item must exist');
//   const blacklist = blacklistPropsFn(api._ignore);
//   const cleaned = blacklist(item);
//   api._updater.modifyItem(cleaned);
//   console.log('save', item);
//   _isUpdating = true;
//   let ret = backend.set(cleaned);
//   _isUpdating = false;
//   return ret;
// };

const updateItem = ({ item, backend, api }) => {
  console.assert(!!item, 'item must exist');
  const blacklist = blacklistPropsFn(api._ignore);
  const cleaned = blacklist(item);
  api._updater.modifyItem(api, cleaned);
  _isUpdating = true;
  let ret = backend.update(cleaned, (data) => api._updater.modifyItem(api, data));
  _isUpdating = false;
  return ret;
};

const removeItem = ({ item, backend, api }) => {
  console.assert(!!item, 'item must exist');
  api._updater.removeItem(api, item);
  console.log('remove', item);
  _isUpdating = true;
  let ret = backend.remove(item);
  _isUpdating = false;
  return ret;
};

/*
  A list of object properties that should not be persisted to firestore,
  allowing the client to store application data.
*/
const defaultIgnore = [
//   'index' // index is only meaningful locally
];

export const create = ({ db, config, ignore = defaultIgnore, backend, path, orderBy, startAt }) => {
  const api = {
    _config: config,
    _database: db,
    _store: {},
    _callbacks: { update: [], load: [], updateDebug: [], loadDebug: [] },
    _ignore: ignore.reduce(mark, {}),
    _loadingState: 'load', // janky enum used separate initial db load from subsequent updates
    _earlyEdits: [], // it's possible to add/save/move before the first load
    _backend: backend.create({ db, path, orderBy, startAt }),
    _loader: loader,
    _updater: updater
  };

  api._backend.onUpdate(onSnapshotFn(api));

  api.all = () => all({ store: api._store });

  api.allFn = (sort) => () => all({ store: api._store, sort });

  api.get = (id) => get({ id, store: api._store });

  api.getFromRemote = (id) => api._backend.getFromRemote(id);

  // ex. api.add({ foo: 'bar' }).to(/* see: chainLocation */).response().then((data) => console.log(data));
//   api.add = (item) => addItem({ item, backend: api._backend, api });

  // fail if doc doesn't exist
//   api.save = (item) => saveItem({ item, backend: api._backend, api });

  api.update = (item) => updateItem({ item, backend: api._backend, api });

  api.remove = (item) => removeItem({ item, backend: api._backend, api });

  api.onLoad = (cb) => hook(api._callbacks.load, cb);

  api.offLoad = (cb) => unhook(api._callbacks.load, cb);

  api.onUpdate = (cb) => hook(api._callbacks.update, cb);

  api.offUpdate = (cb) => unhook(api._callbacks.update, cb);

  api.onLoadDebug = (cb) => hook(api._callbacks.loadDebug, cb);

  api.offLoadDebug = (cb) => unhook(api._callbacks.loadDebug, cb);

  api.onUpdateDebug = (cb) => hook(api._callbacks.updateDebug, cb);

  api.offUpdateDebug = (cb) => unhook(api._callbacks.updateDebug, cb);

  return api;
};

export const _ = {
  defaultIgnore,
  all,
  get,
  chainPromisable
};
