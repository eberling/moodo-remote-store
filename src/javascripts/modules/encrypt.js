// converts array buffer to a string
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}

// converts string to an array buffer
function str2ab(str) {
  var bufView = new Uint8Array(str.length);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return bufView;
}

const salt = () => {
  const val = crypto.getRandomValues(new Uint8Array(16));
  return ab2str(val);
};

const randomString = (length) => {
  const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  const values = new Uint32Array(length);
  crypto.getRandomValues(values);
  for (let i = 0; i < length; i++) {
    result += charset[values[i] % charset.length];
  }
  return result;
};

const key = (password, salt) => {
  if (!password) password = randomString(20);
  const passbuff = new TextEncoder().encode(password);
  const saltVal = str2ab(salt);
  return crypto.subtle.importKey(
    'raw',
    passbuff,
    {name: 'PBKDF2'},
    false,
    ['deriveBits', 'deriveKey']
  ).then((key) => {
    return crypto.subtle.deriveKey(
      {
        'name': 'PBKDF2',
        'salt': saltVal,
        'iterations': 200000,
        'hash': 'SHA-512'
      },
      key,
      { 'name': 'AES-CTR', 'length': 256 },
      true,
      ['encrypt', 'decrypt']
    );
  }).then((webKey) => {
    return webKey;
  });
};

const encrypt = (data, key, salt) => {
  const databuff = new TextEncoder().encode(data);
  const counter = str2ab(salt.slice(0, 16));
  const alg = { name: 'AES-CTR', counter, length: 128 };
  return crypto.subtle.encrypt(alg, key, databuff)
    .then((d) => ab2str(d));
};

const decrypt = (vdata, vkey, vsalt, vcounter) => {
  const salt = str2ab(vsalt);
  const counter = str2ab(vcounter).slice(0, 16);
  const databuff = str2ab(vdata);
  const alg = { name: 'AES-CTR', counter, length: 128 };

  return crypto.subtle.decrypt(alg, vkey, databuff)
    .then(d => ab2str(d));
};

export { salt, key, encrypt, decrypt };
