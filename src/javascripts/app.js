
import * as remoteStore from './modules/remote-store';
import * as crypto from './modules/crypto';
import * as hooks from './modules/hooks';
import * as splitIndex from './modules/split-index';
import * as remoteIdxs from './modules/mixins/remote-index';
import * as childrenById from './modules/mixins/children-by-id';
import * as localIndex from './modules/mixins/local-index';
import * as f from './modules/functional';
import { salt, key, encrypt, decrypt } from './modules/encrypt';

// Firebase App is always required and must be first
import firebase from "firebase/app";
import "firebase/database";

var config = {
  apiKey: "AIzaSyA6aUwQA0XRiodY2gZJuhKet2jEEZdhvEc",
  authDomain: "<PROJECT_ID>.firebaseapp.com",
  databaseURL: "https://moodo-b710c.firebaseio.com/",
  projectId: "moodo-b710c",
  storageBucket: "moodo.appspot.com",
  messagingSenderId: "none"
};

const db = firebase.initializeApp(config);

const rs = remoteStore;

window.firebase = db;

const setup = async () => {
  window.testStore = await rs.create({ config: { uid: 'abc-user', firebase } });
};

window.salt = salt;

window.docs = async () => {
  if (!window.testStore) await setup();
  var api = window.testStore;
  var docs = await new Promise((resolve) => api.loadDocuments(resolve));
  return docs;
};

window.add = async () => {
  if (!window.testStore) await setup();
  var api = window.testStore;
  api.addDocument({
    id: 'abc',
    p: { d: firebase.database.ServerValue.TIMESTAMP, t: 'doc title or something' }
  }, [
    { id: 'item1', data: 'y' },
    { id: 'item2', data: 'x' }
  ]);
};

window.abcItems = async () => {
  if (!window.testStore) await setup();
  var api = window.testStore;
  return api.document('abc').items;
};
window.doCrypto = async () => {
  const c = window.myCrypto = crypto.create();

  await c.add({ name: 'a' });
  await c.add({ name: 'b', password: 'b-pw' });
  await c.add({ name: 'c', password: 'c-pw', sentinel: 'c-sentinel' });
  const acrypt1 = await c.encryptText('a', 'something');
  const acrypt2 = await c.encryptText('a', 'something');
  const ashow1 = await c.decryptText('a', acrypt1);
  const ashow2 = await c.decryptText('a', acrypt2);
  console.log('a', acrypt1, acrypt2, ashow1, ashow2);

  const acrypt3 = await c.encryptText('a', 'else');
  const ashow3 = await c.decryptText('a', acrypt3);
  console.log('a', acrypt3, ashow3);

  await c.add({ name: 'b-same', password: 'b-pw' });
  const bcrypt1 = await c.encryptText('b', 'something');
  const bSamecrypt1 = await c.encryptText('b-same', 'something');
  console.log('same pw, same results', bcrypt1 === bSamecrypt1);

  await c.delete({ name: 'a' });
  try {
    const acrypt4 = await c.encryptText('a', 'something');
  } catch (e) {
    console.log('yay error', e, e instanceof Error, e instanceof crypto.InvalidArgumentError);
  }

  await c.add({ name: 'a' });
  const canEncrypt = await c.canEncrypt('a', await c.encryptText('a', 'sentinel'));
  console.log('canEncrypt', canEncrypt);

  console.log('verify good', await c.verify({ name: 'b', password: 'b-pw' }));
  console.log('verify bad', await c.verify({ name: 'b', password: 'b-wrong' }));
};
