

After installing dependencies, run the blendid app to test in the browser:

```
yarn run blendid
```

Explore the "api" variable on the command line.
